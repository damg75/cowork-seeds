class Contract < ActiveRecord::Base
    belongs_to :movie
    belongs_to :person

    # validates :movie_id, :person_id, presence: true
    validates :movie_id, presence: {message: 'Tienes que tener una pelicula'}
end