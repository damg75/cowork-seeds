class Genre < ActiveRecord::Base
    has_many :movies
    has_many :production_companies, through: :movies
end