class Movie < ActiveRecord::Base
    belongs_to :genre
    belongs_to :production_company
    has_many :contracts
    has_many :people, through: :contracts
end