class Person < ActiveRecord::Base
    belongs_to :country
    has_many :contracts
    has_many :movies, through: :contracts

    validates :name, presence: true
end