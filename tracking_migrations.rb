#Migrations tracking

rake db:new_migration name=CreateGenres options="name:string" #crea la tabla Genre

t.timestamp

rake db:migrate

rake db:new_migration name=CreateCountries options="name:string" #crea la tabla Country

rake db:new_migration name=CreateProduction_Companies options="name:string rif:string" #crea la tabla Production Companies

t.timestamp

rake db:migrate

rake db:new_migration name=CreatePeople options="name:integer last_name:integer Email:string male:boolean birthdate:date country:references" #crea la tabla People

t.timestamp

rake db:migrate

rake db:new_migration name=CreateMovies options="title:string release_date:date rating:decimal description:string runtime:integer budget:decimal" #crea la tabla Movie

rake db:new_migration name=AddProduction_CompanyRefToMovies options="production_company:references" #Crea la referencia production_company_id en Movies

rake db:new_migration name=AddGenreRefToMovies options="genre:references" #Crea la referencia genre_id en Movies

t.timestamp

rake db:migrate

rake db:new_migration name=CreateContracts options="role:string price:decimal start_date:date end_date:date" #crea la tabla Contracts

rake db:new_migration name=AddMovieRefToContracts options="movie:references" #crea la referencia movie_id en Contracts

rake db:new_migration name=AddPersonRefToContracts options="person:references" #crea la referencia people_id en Contracts

t.timestamp

rake db:migrate