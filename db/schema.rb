# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_11_20_154153) do

  create_table "contracts", force: :cascade do |t|
    t.string "role"
    t.decimal "price"
    t.date "start_date"
    t.date "end_date"
    t.integer "movie_id"
    t.integer "person_id"
    t.index ["movie_id"], name: "index_contracts_on_movie_id"
    t.index ["person_id"], name: "index_contracts_on_person_id"
  end

  create_table "countries", force: :cascade do |t|
    t.string "name"
  end

  create_table "genres", force: :cascade do |t|
    t.string "name"
  end

  create_table "movies", force: :cascade do |t|
    t.string "title"
    t.date "release_date"
    t.decimal "rating"
    t.string "description"
    t.integer "runtime"
    t.decimal "budget"
    t.integer "production_company_id"
    t.integer "genre_id"
    t.index ["genre_id"], name: "index_movies_on_genre_id"
    t.index ["production_company_id"], name: "index_movies_on_production_company_id"
  end

  create_table "people", force: :cascade do |t|
    t.integer "name"
    t.integer "last_name"
    t.string "Email"
    t.boolean "male"
    t.date "birthdate"
    t.integer "country_id"
    t.index ["country_id"], name: "index_people_on_country_id"
  end

  create_table "production_companies", force: :cascade do |t|
    t.string "name"
    t.string "rif"
  end

end
