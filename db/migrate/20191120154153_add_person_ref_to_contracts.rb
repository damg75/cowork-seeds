class AddPersonRefToContracts < ActiveRecord::Migration[5.2]
  def change
    add_reference :contracts, :person, foreign_key: true
  end
end
