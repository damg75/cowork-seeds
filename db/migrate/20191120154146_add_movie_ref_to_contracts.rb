class AddMovieRefToContracts < ActiveRecord::Migration[5.2]
  def change
    add_reference :contracts, :movie, foreign_key: true
  end
end
