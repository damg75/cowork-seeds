class CreateProductionCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :production_companies do |t|
      t.string :name
      t.string :rif
      t.timestamp
    end
  end
end
