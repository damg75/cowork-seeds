class CreateContracts < ActiveRecord::Migration[5.2]
  def change
    create_table :contracts do |t|
      t.string :role
      t.decimal :price
      t.date :start_date
      t.date :end_date
      t.timestamp
    end
  end
end
