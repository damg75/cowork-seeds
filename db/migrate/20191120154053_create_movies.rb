class CreateMovies < ActiveRecord::Migration[5.2]
  def change
    create_table :movies do |t|
      t.string :title
      t.date :release_date
      t.decimal :rating
      t.string :description
      t.integer :runtime
      t.decimal :budget
      t.timestamp
    end
  end
end
