class CreatePeople < ActiveRecord::Migration[5.2]
  def change
    create_table :people do |t|
      t.integer :name
      t.integer :last_name
      t.string :Email
      t.boolean :male
      t.date :birthdate
      t.references :country, foreign_key: true
      t.timestamp
    end
  end
end
