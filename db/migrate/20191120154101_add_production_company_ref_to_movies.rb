class AddProductionCompanyRefToMovies < ActiveRecord::Migration[5.2]
  def change
    add_reference :movies, :production_company, foreign_key: true
  end
end
